# The Xenin Framework

A framework developed for Garry's Mod, intended to make mundane tasks just a bit easier and more interconnected.

**Major features:**
- SQL query builder
- SQL migrations
- MySQL/SQLite support
- Easily integrable in-game config system
- Tons of pre-built UI panels
- Lots of helper functions
- Permissions interface

---

* **GitLab:** https://gitlab.com/sleeppyy/xeninui/
* **Steam Workshop:** https://steamcommunity.com/sharedfiles/filedetails/?id=1900562881